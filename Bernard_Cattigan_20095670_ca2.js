//  Name: Bernard Cattigan
//  Student_id: 20095670
//  Databases Assignment 2


//  1.  Read (Find)


//  1a. Find all 90's mystery movies that are not in english
//      returning just the title, year, genre and language
//      sorted by year in ascending order

db.movies.find(
    {
        year: { $gt: 1991, $lte: 1999 },
        genres: "Mystery",
        languages: { $nin: [("English")] }
    },
    { _id: 0, title: 1, year: 1, genres: 1, languages: 1 }
).sort({ year: 1 }).pretty()


//  1b. Find movies directed by Clint Eastwood
//      that have a comment since 2015
//      returning just the title, year, directors and comment date
//      limited to 5 results

db.movies.find(
    {
        directors: "Clint Eastwood",
        comments: {
            $elemMatch: {
                date: { $gte: new ISODate("2015-01-01T00:00:00Z") }
            }
        }
    },
    { _id: 0, title: 1, year: 1, directors: 1, "comments.date": 1 }
).limit(5).pretty()


//  1c. Find movies with tomatoes rating less than or equal to 0
//      returning just the title, tomatoes rating and tomatoes number of reviews
//      sorted by total number of reviews and limited to 5 results

db.movies.find(
    {
        "tomatoes.viewer.rating": { $lte: 0 },
    },
    { _id: 0, title: 1, "tomatoes.viewer.rating": 1, "tomatoes.viewer.numReviews": 1 }
).sort({ "tomatoes.viewer.numReviews": -1 }).limit(5).pretty()


//  1d. Find movies with a runtime of less than 100 minutes that are a comedy and star Brad Pitt
//      returning just the title, runtime, cast, tomatoes rating and imbd rating
//      sorted by tomatoes and IMBD rating in descending order

db.movies.find(
    {
        runtime: { $lte: 100 },
        genres: "Comedy",
        cast: {
            $in: [("Brad Pitt")],
        }
    },
    { _id: 0, title: 1, runtime: 1, cast: 1, "tomatoes.viewer.rating": 1, "imdb.rating": 1 }
).sort({ "tomatoes.viewer.rating": -1, "imdb.rating": -1 }).pretty()


//  1e. Find movies where writers names conatins Cohen and is available in English and French
//      returning just the title, year, writers and languages
//      skipping first 4 results

db.movies.find(
    {
        writers: { $regex: "Cohen" },
        languages: { $all: ["French", "English"] }
    },
    { _id: 0, title: 1, year: 1, writers: 1, languages: 1 }
).skip(4).pretty()


//  1f. Find movies bewteen 2000 and 2010 taht Bruce Caldwell commented on 
//      returning just the title, year and comments details (comments.$ means it only shows comments from Bruce Caldwell)
//      sorted by year, limited to 10 results

db.movies.find(
    {
        year: { $gt: 2000, $lte: 2010 },
        comments: {
            $elemMatch: {
                name: "Bruce Caldwell"
            }
        }
    },
    { _id: 0, title: 1, year: 1, "comments.$": 1 }
).limit(10).sort({ year: -1 }).pretty()


//  2.  Create (Insert)


//  2a. Movies
db.movies.insertMany(
    [
        {
            "_id": "BC001",
            "title": "Okja",
            "year": 2017,
            "runtime": 120,
            "cast": ["Tilda Swinton", "Paul Dano", "Seo-hyun Ahn"],
            "plot": "A young girl risks everything to prevent a powerful, multinational company from kidnapping her best friend - a fascinating beast named Okja",
            "directors": ["Bong Joon Ho"],
            "imbd": { "rating": 7.3, "votes": 115, "genres": ["Action", "Adventure", "Drama"] }
        },
        {
            "_id": "BC002",
            "title": "Avengers: Infinity War",
            "year": 2018,
            "runtime": 149,
            "cast": ["Robert Downey Jr", "Chris Hemsworth", "Mark Ruffalo"],
            "plot": "The group formulates a plan to subdue Thanos and remove the Infinity Gauntlet, which he uses to safely house the Stones.",
            "directors": ["Anthony Russo", "Joe Russo"],
            "imbd": { "rating": 8.4, "votes": 963, "genres": ["Action", "Adventure", "Sci-Fi"] }
        },
        {
            "_id": "BC003",
            "title": "Joker",
            "year": 2019,
            "runtime": 122,
            "cast": ["Joaquin Phoenix", "Robert De Niro", "Zazie Beetz"],
            "plot": "Origin story of Batman villain the Joker",
            "directors": ["Todd Phillips"],
            "imbd": { "rating": 8.4, "votes": 1100, "genres": ["Crime", "Drama", "Thriller"] }
        }
    ]
)

//  check
db.movies.find(
    {
        _id: { $in: ["BC001", "BC002", "BC003"] }
    }
).pretty()


//  2a. Users

db.users.insertMany(
    [
        {
            "_id": "USER001",
            "firstName": "Bernard",
            "surname": "Cattigan",
            "email": "b.cattigan@mail.com",
            "age": 34,
            "password": "password1",
            "favourites": [
                { "_id": "BC001" },
                { "_id": { "$oid": "573a1390f29313caabcd5ca5" } },
                { "_id": { "$oid": "573a1390f29313caabcd4622" } }
            ]
        },
        {
            "_id": "USER002",
            "firstName": "Michelle",
            "surname": "Cattigan",
            "email": "m.cattigan@mail.com",
            "age": 34,
            "password": "password2",
            "favourites": [
                { "_id": "BC002" },
                { "_id": "BC003" },
                { "_id": { "$oid": "573a1391f29313caabcd73bf" } },
                { "_id": { "$oid": "573a1391f29313caabcd7c9e" } }
            ]
        },
    ]
)

//  check
db.users.find(
    {
        _id: { $in: ["USER001", "USER002"] }
    }
).pretty()


//  3.  Update


//  3a. Update the IMDB rating to a new value and increase the number of votes by 1.

db.movies.updateOne(
    { "_id": "BC001" },
    {
        $set: { "imbd.rating": 9.0 },
        $inc: { "imbd.votes": 1 }
    }
)

// Reference: https://www.w3resource.com/mongodb/mongodb-field-update-operator-$inc.php

//  check
db.movies.find(
    { _id: "BC001" }
).pretty()


// 3b. Add a tomatoes subdocument to one of your movies.

db.movies.updateOne(
    { "_id": "BC002" },
    {
        $set: {
            "tomatoes": {
                "viewer": {
                    "rating": 3.0,
                    "numReviews": 184,
                    "meter": 32
                },
                "lastUpdated": { "$date": "2015-06-28T18:34:09Z" }
            }
        }
    },
)

//  check
db.movies.find(
    { _id: "BC002" }
).pretty()


// 3c. Add a new favourite to the array in one of your user documents.

db.users.updateOne(
    { "_id": "USER001" },
    {
        $push: {
            favourites: { "_id": "BC002" }
        }
    }
)

//  check
db.users.find(
    { _id: "USER001" }
).pretty()


// 3d. Additional update of your choice. (Using Upsert)

db.users.updateOne(
    { "_id": "USER003", },
    {
        $set: {
            "firstName": "Missy",
            "surname": "Cattigan",
            "email": "missy.cattigan@mail.com",
            "age": 34,
            "password": "password3",
            "favourites": [
                { "_id": "BC001" },
                { "_id": "BC002" },
                { "_id": "BC003" },
            ]
        },
    },
    { upsert: true }
)

//  check
db.users.find(
    { _id: "USER003" }
).pretty()


// 3e. Additional update of your choice. (Using updateMany)

db.movies.updateMany(
    { _id: { $in: ["BC001", "BC002", "BC003"] } },
    { $set: { year: 2022 } }
)

//  check
db.movies.find(
    {
        _id: { $in: ["BC001", "BC002", "BC003"] }
    }
).pretty()


//  4.  Delete

db.movies.deleteOne({ _id: "BC003" })

//check
db.movies.find(
    { _id: "BC003" }
).pretty()
